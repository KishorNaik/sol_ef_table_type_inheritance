﻿using Sol_EF_Table_Type_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Type_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<EmployeeEntity> listEmployeeObj =
                   await new EmployeeRepository().GetEmployeeData();

              

                IEnumerable<VendorEntity> listVendorObj =
                    await new VendorRepository().GetVendorData();

            }).Wait();
        }
    }
}
