﻿using Sol_EF_Table_Type_Inheritance.EF;
using Sol_EF_Table_Type_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Type_Inheritance
{
    public class VendorRepository : PersonRepository
    {
        //#region Declaration
        //private TestDBEntities db = null;
        //#endregion

        //#region Constructor
        //public VendorRepository()
        //{
        //    db = new TestDBEntities();
        //}
        //#endregion

        #region Constructor
        public VendorRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<VendorEntity>> GetVendorData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                       base.DbObject
                        ?.tblPersons
                        ?.OfType<tblVendor>()
                        ?.AsEnumerable()
                        ?.Select(this.SelectEmployeeData)
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tblVendor, VendorEntity> SelectEmployeeData
        {
            get
            {
                return
                    (leTblVendorObj) => new VendorEntity()
                    {
                        PersonId = leTblVendorObj.PersonId,
                        FirstName = leTblVendorObj.FirstName,
                        LastName = leTblVendorObj.LastName,
                        Wages = leTblVendorObj.Wages
                    };
            }
        }

        #endregion
    }
}
