﻿using Sol_EF_Table_Type_Inheritance.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Type_Inheritance
{
    public class PersonRepository
    {
        #region Declaration
        private TestDBEntities db = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            db = new TestDBEntities();

            this.DbObject = db;
        }
        #endregion

        #region Property
        public TestDBEntities DbObject { get; set; }
        #endregion
    }
}
