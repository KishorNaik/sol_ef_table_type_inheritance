﻿using Sol_EF_Table_Type_Inheritance.EF;
using Sol_EF_Table_Type_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Type_Inheritance
{
    public class EmployeeRepository : PersonRepository
    {
        //#region Declaration
        //private TestDBEntities db = null;
        //#endregion

        //#region Constructor
        //public EmployeeRepository()
        //{
        //    db = new TestDBEntities();
        //}
        //#endregion

        #region Constructor
        public EmployeeRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<EmployeeEntity>> GetEmployeeData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        base.DbObject
                        ?.tblPersons
                        ?.OfType<tblEmployee>()
                        ?.AsEnumerable()
                        ?.Select(this.SelectEmployeeData)
                        ?.ToList();

                    return getQuery;

                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<tblEmployee, EmployeeEntity> SelectEmployeeData
        {
            get
            {
                return
                    (leTblEmployeeObj) => new EmployeeEntity()
                    {
                        PersonId = leTblEmployeeObj.PersonId,
                        FirstName = leTblEmployeeObj.FirstName,
                        LastName = leTblEmployeeObj.LastName,
                        Salary = leTblEmployeeObj.Salary
                    };
            }
        }

        #endregion
    }
}
